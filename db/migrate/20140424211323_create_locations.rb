class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :zip,         :limit => 10, :null => false
      t.string :description, :limit => 80

      t.timestamps
    end
    add_index :locations, :zip
  end
end
