class CreateZipCodes < ActiveRecord::Migration
  def change
    create_table :zip_codes do |t|
      t.string :zip, :limit => 20
      t.string :location, :limit => 40
      t.timestamps
    end
  end
end
