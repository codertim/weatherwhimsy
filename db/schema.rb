# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140424211323) do

  create_table "locations", force: true do |t|
    t.string   "zip",         limit: 10, null: false
    t.string   "description", limit: 80
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["zip"], name: "index_locations_on_zip"

  create_table "zip_codes", force: true do |t|
    t.string   "zip",        limit: 20
    t.string   "location",   limit: 40
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
