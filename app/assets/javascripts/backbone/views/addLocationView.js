
WeatherWhimsy.Views.AddLocationView = Backbone.View.extend({
  className: 'location span-15',

  initialize: function() {
    _.bindAll(this, 'render');
  },

  events: {
    // 'submit': 'submitForm',
    'click button': 'submitForm'
  },

  render: function() {
    $(this.el).html(WeatherWhimsy.template('addLocationViewTemplate').render());
    $(this.el).attr("id", "addlocation");
    return this;
  },

  submitForm: function(event) {
    alert("VIEW - AddLocationView#submitForm - Starting ...");
    var locationZipInput = this.$el.find('input[id="locationzip"]').val();
    alert("You entered: " + locationZipInput);
    var zipCode = new WeatherWhimsy.Models.ZipCode();
    var zipCodeDetails = {
      'zip': locationZipInput
    };
    zipCode.save(zipCodeDetails, {
      success: function(zipCode) {
        // alert("zipCode.id = " + zipCode.get("id"));
        WeatherWhimsy.app.navigate("", {trigger: true});
      }
    });
  }

});


