
WeatherWhimsy.Views.SidebarView = Backbone.View.extend({
  tagName: 'section',
  className: 'span-4',
  id: 'my-sidebar',

  initialize: function(){
    _.bindAll(this, 'render', 'goToAddLocationPage');
  },

  events: {
    'click #add_location_link': 'goToAddLocationPage',
  },

  render: function(){ 
    this.$el.html(
        WeatherWhimsy.template('sidebarViewTemplate').render());
    return this;
  },

  goToAddLocationPage: function(event) {
    // alert("SidebarView#goToAddLocationPage");
    event.preventDefault();   // return true same thing? TODO: check out
    WeatherWhimsy.app.navigate("addlocation", true);
  }
});


