
WeatherWhimsy.Views.TopNavigationView = Backbone.View.extend({
  tagName: 'nav',
  className: 'topNav',

  initialize: function() {
     _.bindAll(this, 'render');
  },

  render: function () {
    this.$el.html(
        WeatherWhimsy.template('topNavigationViewTemplate').render());
    return this;
  }
});

