
WeatherWhimsy.Views.ZipCodesView = Backbone.View.extend({
  tagName: 'section',
  className: 'all-zipcodes span-20',

  initialize: function () {
    _.bindAll(this, 'render', 'renderZipCode');
    this.collection.bind('reset', this.render, this);
  },

  render: function() {
    this.$el.html(WeatherWhimsy.template('zipCodesViewTemplate').render());
    var $zipCodes = this.$('.zipcodes');
    console.log("ZipCodesView#render - this.collection.length = " + this.collection.length);
    this.collection.each(function(zipCodeModel, index) {
      $zipCodes.append(this.renderZipCode(zipCodeModel).el);
    }, this);
    return this;
  },

  renderZipCode: function(zipCodeModel) { 
    var view = new WeatherWhimsy.Views.ZipCodeView( { model: zipCodeModel, collection: this.collection} );
    return view.render();
  }
});


