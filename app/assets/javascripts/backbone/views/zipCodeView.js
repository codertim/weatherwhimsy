
WeatherWhimsy.Views.ZipCodeView = Backbone.View.extend({
  className: 'zipcode zipcode_entry span-6',

  initialize: function () {
    _.bindAll(this, 'render', 'displayDetails', 'goToDetailPage');
    this.model.bind('change:detailsDisplayed', this.displayDetails, this);
  },

  render: function () {
    console.log("this.model.attributes = " + this.model.attributes);
    this.$el.html(WeatherWhimsy.template('zipCodeViewTemplate').render(this.model.attributes));
    this.displayDetails();   // logic to decide to display details or not
    return this;
  },

  events: {
    'click .zipcode_detail_link': 'toggleDetails',
    'click .detail_page_link': 'goToDetailPage'
  },

  goToDetailPage: function(event) {
    alert("ZipCodeView#goToDetailPage - Starting ...");
    event.preventDefault();
    WeatherWhimsy.app.navigate("zipcodes/detail/" + this.model.get("id"), true);
  },

  displayDetails: function() {
    if(this.model.get("detailsDisplayed")) {
      this.$el.find(".zipcode_detail_link").html("Hide Details");
      this.$el.find(".zipcode_details").removeClass("hidden");
    } else {
      this.$el.find(".zipcode_detail_link").html("Show Details");
      this.$el.find(".zipcode_details").addClass("hidden");
    }
    // this.render();   not in mstwj book
    // alert("Fetching ...");
    // WeatherWhimsy.zipCodes.fetch();
  },

  toggleDetails: function(event) {
    this.model.toggleDetails();
  }

});
