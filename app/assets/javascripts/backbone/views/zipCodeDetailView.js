
WeatherWhimsy.Views.ZipCodeDetailView = Backbone.View.extend({
  className: 'zipcode span-20',

  render: function() {
    $(this.el).html(WheatherWhimsy.template('zipCodeDetailViewTemplate').render(this.model.toJSON()));
    $(this.el).attr("id", "zipcode_detail_" + this.model.get("id"));
    return this;
  },
});
