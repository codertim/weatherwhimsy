
WeatherWhimsy.Models.ZipCode = Backbone.Model.extend({
  urlRoot: "/locations",

  defaults: {
    'detailsDisplayed': false
  },

  toggleDetails: function() {
    var currentDetailsDisplayed = this.get('detailsDisplayed');
    // alert("MODEL#toggleDetails - currentDetailsDisplayed = " + currentDetailsDisplayed);

    if(currentDetailsDisplayed == false) {
      this.set({'detailsDisplayed': true});
    } else {
      this.set({'detailsDisplayed': false});
    }
  }

});

WeatherWhimsy.Collections.ZipCodes = Backbone.Collection.extend({
  model: WeatherWhimsy.Models.ZipCode,
  url: "/zipcodes"
});


