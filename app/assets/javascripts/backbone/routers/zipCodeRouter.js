WeatherWhimsy.Routers.ZipCodeRouter = Backbone.Router.extend({
  routes : {
    "": "index",
    "zipcodes/detail/:id": "zipCodeDetail",
    "addlocation": "addLocation",
  },

  index: function () {
    $("#container").empty();
    $container = $("#container");
    $container.append(this.sidebarView.render().el);
    $container.append(this.topNavigationView.render().el);

    // main content
    var $content = $("<div/>").attr("id", "content");
    $container.append($content);
    // console.log("WeatherWhimsy.zipCodes = " + WeatherWhimsy.zipCodes.toString());
    var zipCodesView = new WeatherWhimsy.Views.ZipCodesView({collection: WeatherWhimsy.zipCodes});
    // console.log("zipCodesView: " + zipCodesView.collection.toString());
    // console.log("zipCodesView: "); console.dir(zipCodesView);
    $content.append(zipCodesView.render().el);
  },

  initialize: function() {
    this.topNavigationView = new WeatherWhimsy.Views.TopNavigationView();
    this.sidebarView = new WeatherWhimsy.Views.SidebarView();
  },

  zipCodeDetail: function(id) {
    // alert("ROUTER - zipCodeDetail - Starting - param id = " + id);

    $("section.all-zipcodes").remove();   // delete list of multiple zip codes

    $container = $("#container");
    $container.append(this.sidebarView.render().el);
    $container.append(this.topNavigationView.render().el);

    var location = WeatherWhimsy.zipCodes.get(id);   // lookup specific model in collection
    console.log("location id = " + location.get("id"));
    var $content = $("<div />").attr("id", "content");
    $content.append("<br />ID: " + location.get("id"));
    $content.append("<br />Zip Code: " + location.get("zip"));
    $content.append("<br />Location: " + location.get("description"));
    $container.append($content);
    // var zipCodeDetailView = new WeatherWhimsy.Views.ZipCodeDetailView({ model: WheatherWhimsy.getZipCode(id)});
    // $content.append(zipCodeDetailView.render().el);
    return $container;
  },

  addLocation: function() {
    alert("ROUTER#addLocation - Starting ...");
    $("section.all-zipcodes").remove();   // delete list of multiple zip codes
    $("#container").empty();
    $container = $("#container");    
    $container.append(this.sidebarView.render().el);
    $container.append(this.topNavigationView.render().el);

    var $content = $("<div />").attr("id", "content");
    $content.append("<h2>Add Location</h2>");
    $container.append($content);

    var addLocationView = new WeatherWhimsy.Views.AddLocationView();
    $container.append(addLocationView.render().el);

    return $container;
  }
});


