//= require_self
//= require_tree ./templates
//= require_tree ./models
//= require_tree ./views
//= require_tree ./routers

var WeatherWhimsy = {
  Models: {},
  Collections: {},
  Routers: {},
  Views: {},

  init: function(zipCodeData) {
    // console.log("WeatherWhimsy#init - PARAMS - zipCodeData = " + zipCodeData.toSource());
    this.zipCodes = new WeatherWhimsy.Collections.ZipCodes(zipCodeData);
    this.app = new WeatherWhimsy.Routers.ZipCodeRouter();
    Backbone.history.start({pushState: true});
    return this;
  },

  template: function(filename) {
    return HoganTemplates["backbone/templates/" + filename];
  },

  getZipCode: function(id) {
    alert("WeatherWhimsy#getZipCode - Starting ...");
    return this.zipCodes.find(function (zipCode) { 
      return zipCode.get("id").toString() === id.toString();
    })
  }
}

