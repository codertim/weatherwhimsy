class HomeController < ApplicationController
  respond_to :html, :json

  def index
    @zip_codes = Location.all
    logger.debug("##### HomeController#index - zip codes = #{@zip_codes.inspect}")
    # debugger
    respond_with(@zip_codes)
  end
end

