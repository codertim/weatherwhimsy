class LocationsController < ApplicationController
  # respond_to :json

  # def show
  #   @location = Location.find(params[:id])
  #   respond_with @location
  # end

  def create
    logger.debug("##### LocationsController#create - Starting ... - params[:location]=" + params[:location].inspect)
    @location = Location.new(params[:location].permit(:zip))

    if @location.save
      logger.info("##### LocationsController#create - created new location")
      response_hash = { "id" => @location.id }
      render :json => response_hash
    else
      logger.error("##### LocationsController#create - ERROR - could not create new location with params=" + params.inspect)
    end
  end
end
